#include "Renderer.h"

#include "Logging.h"

#define IfFailedReturnHR(expr) {HRESULT hr = (expr); if (FAILED(hr)) return hr;}

HRESULT Renderer::CreateDeviceResources(HWND hwnd)
{
    // Create a D2D render target properties
    D2D1_RENDER_TARGET_PROPERTIES renderTargetProperties = D2D1::RenderTargetProperties();

    RECT rect;
    if (!GetClientRect(hwnd, &rect)) {
        logging::DebugPrintf(TEXT("Failed to get client rect dimensions"));
        return false;
    }

    // Create a D2D render target
    D2D1_SIZE_U size = D2D1::SizeU(rect.right - rect.left, rect.bottom - rect.top);
    IfFailedReturnHR(D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &m_pD2DFactory));
    IfFailedReturnHR(m_pD2DFactory->CreateHwndRenderTarget(
        renderTargetProperties,
        D2D1::HwndRenderTargetProperties(hwnd, size),
        &m_pRT
    ));

    return S_OK;
}

HRESULT Renderer::Render(HWND hwnd, IWICBitmapSource *image) {
    // Do I need to check that hwnd matches?
    if (!m_pRT) {
        IfFailedReturnHR(CreateDeviceResources(hwnd));
    }
    PAINTSTRUCT ps;
    BeginPaint(hwnd, &ps);
    m_pRT->BeginDraw();
    m_pRT->SetTransform(D2D1::Matrix3x2F::Identity());
    m_pRT->Clear(D2D1::ColorF(D2D1::ColorF::White));

    CComPtr<ID2D1Bitmap> pD2DBitmap;
    D2D1_SIZE_F rtSize = m_pRT->GetSize();
    D2D1_RECT_F rectangle = D2D1::RectF(0.0f, 0.0f, rtSize.width, rtSize.height);
    // can cache this
    m_pRT->CreateBitmapFromWicBitmap(image, NULL, &pD2DBitmap);
    m_pRT->DrawBitmap(pD2DBitmap, rectangle);
    m_pRT->EndDraw();
    EndPaint(hwnd, &ps);
    return S_OK;
}
