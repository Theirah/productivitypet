#include <atlbase.h>
#include <wincodec.h>
#include <windows.h>

#include "Logging.h"
#include "Pet.h"
#include "Renderer.h"
#include "WICFactorySingleton.h"

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
const int WINDOW_HEIGHT = 248;
const int WINDOW_WIDTH = 248;
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, PWSTR pCmdLine, int nCmdShow)
{
    // Register the window class.
    const wchar_t CLASS_NAME[] = L"Sample Window Class";
    WNDCLASS wc = {};

    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.lpszClassName = CLASS_NAME;

    RegisterClass(&wc);

    HWND hwnd = CreateWindowEx(
        WS_EX_LAYERED,                              // Optional window styles.
        CLASS_NAME,                     // Window class
        nullptr,    // Window text
        WS_OVERLAPPEDWINDOW,            // Window style

                                        // Size and position
        100, 50, WINDOW_WIDTH, WINDOW_HEIGHT,

        NULL,       // Parent window    
        NULL,       // Menu
        hInstance,  // Instance handle
        NULL        // Additional application data
    );

    if (hwnd == NULL)
    {
        return 0;
    }
    SetLayeredWindowAttributes(hwnd, RGB(255,255,255), 0, LWA_COLORKEY);
    ShowWindow(hwnd, nCmdShow);
    MSG msg = {};
    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    return 0;
}


LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;
    case WM_SIZING:
    case WM_PAINT:
    {
        CComPtr<IWICFormatConverter> image;
        try {
            image = pet::GetImage();
        }
        catch (HRESULT hr) {
            logging::DebugPrintHResult(hr);
            return 0;
        }
        Renderer renderer;
        renderer.Render(hwnd, image);
    }

    return 0;

    }
    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}
