#include <atlbase.h>
#include <wincodec.h>
#include <memory>

#ifndef WIC_FACTORY_SINGLETON_H
#define WIC_FACTORY_SINGLETON_H
class WICFactorySingleton {
public:
    inline static WICFactorySingleton& GetInstance()
    {
        if (nullptr == m_pInstance.get())
            m_pInstance.reset(new WICFactorySingleton());
        return *m_pInstance;
    }

    virtual IWICImagingFactory* GetFactory() const;

protected:
    IWICImagingFactory* m_pWICImagingFactory;

private:
    WICFactorySingleton();   // Private because singleton
    static std::shared_ptr<WICFactorySingleton> m_pInstance;
};
#endif // WIC_FACTORY_SINGLETON_H