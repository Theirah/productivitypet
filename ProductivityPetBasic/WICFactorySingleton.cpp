#include "stdafx.h"
#include "WICFactorySingleton.h"
#include <assert.h>

std::shared_ptr<WICFactorySingleton> WICFactorySingleton::m_pInstance;

WICFactorySingleton::WICFactorySingleton()
    : m_pWICImagingFactory(nullptr)
{
    HRESULT hr = CoCreateInstance(CLSID_WICImagingFactory, NULL, CLSCTX_INPROC_SERVER,
        IID_IWICImagingFactory, (LPVOID*) &m_pWICImagingFactory);
    assert(SUCCEEDED(hr));
}

IWICImagingFactory* WICFactorySingleton::GetFactory()  const
{
    assert(m_pWICImagingFactory);
    return m_pWICImagingFactory;
}