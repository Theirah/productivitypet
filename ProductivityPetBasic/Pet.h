#include <atlbase.h>
#include <wincodec.h>

#ifndef PET_H
#define PET_H
namespace pet {
    CComPtr<IWICFormatConverter> GetImage();
}
#endif // PET_H