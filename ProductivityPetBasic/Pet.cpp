#include "Pet.h"

#include <assert.h>
#include <atlbase.h>
#include <wincodec.h>

#include "WICFactorySingleton.h"

#define IfFailedThrowHR(expr) {HRESULT hr = (expr); if (FAILED(hr)) throw hr;}

namespace pet {
    CComPtr<IWICFormatConverter> GetImage()
    {
        const wchar_t* pszFile = LR"(D:\Documents\Brewtopia\Brewtopia_two_rims_nontransparent.gif)";
        UINT nFrame = 0;

        CComPtr<IWICBitmapDecoder> pDecoder;
        CComPtr<IWICBitmapFrameDecode> pFrame;
        CComPtr<IWICFormatConverter> pConvertedFrame;

        // Get the WIC factory from the singleton wrapper class
        IWICImagingFactory* pFactory = WICFactorySingleton::GetInstance().GetFactory();
        assert(pFactory);
        if (!pFactory)
            throw WINCODEC_ERR_NOTINITIALIZED;

        // Create a decoder for the given image file
        IfFailedThrowHR(pFactory->CreateDecoderFromFilename(
            pszFile, NULL, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &pDecoder));
            
        // Validate the given frame index nFrame
        UINT nCount = 0;
        // Get the number of frames in this image
        if (SUCCEEDED(pDecoder->GetFrameCount(&nCount)))
        {
            if (nFrame >= nCount)
                nFrame = nCount - 1; // If the requested frame number is too big, default to the last frame
        }
        // Retrieve the requested frame of the image from the decoder
        IfFailedThrowHR(pDecoder->GetFrame(nFrame, &pFrame));

        // Convert the format of the image frame to 32bppBGR
        IfFailedThrowHR(pFactory->CreateFormatConverter(&pConvertedFrame));
        IfFailedThrowHR(pConvertedFrame->Initialize(
            pFrame,                        // Source frame to convert
            GUID_WICPixelFormat32bppPBGRA,     // The desired pixel format
            WICBitmapDitherTypeNone,         // The desired dither pattern
            NULL,                            // The desired palette 
            0.f,                             // The desired alpha threshold
            WICBitmapPaletteTypeCustom       // Palette translation type
        ));
        return pConvertedFrame;
    }
}
