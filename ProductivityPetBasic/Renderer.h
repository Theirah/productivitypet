#include <atlbase.h>
#include <d2d1.h>

#ifndef RENDERER_H
#define RENDERER_H

class Renderer {
public:
    HRESULT CreateDeviceResources(HWND hwnd);
    HRESULT Render(HWND hwnd, IWICBitmapSource *image);
private:
    CComPtr<ID2D1Factory> m_pD2DFactory;
    CComPtr<ID2D1HwndRenderTarget>  m_pRT;
};
#endif // RENDERER_H