#include <wincodec.h>

#ifndef LOGGING_H
#define LOGGING_H
namespace logging {
    void DebugPrintf(const wchar_t * str, ...);
    void DebugPrintHResult(HRESULT hr);
}
#endif // LOGGING_H