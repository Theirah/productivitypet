#include "Logging.h"

#include <windows.h>
#include <stdarg.h>
#include <stdio.h>

namespace logging {
    void DebugPrintf(const wchar_t * str, ...) {
        wchar_t buffer[1024];
        va_list args;
        va_start(args, str);
        vswprintf(buffer, 1024, str, args);
        OutputDebugString(buffer);
    }

    void DebugPrintHResult(HRESULT hr) {
        DebugPrintf(TEXT("Hresult facility: %i, code: %i: "), HRESULT_FACILITY(hr), HRESULT_CODE(hr));

        TCHAR* szErrMsg;

        if (FormatMessage(
            FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
            NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
            (LPTSTR)&szErrMsg, 0, NULL) != 0)
        {
            logging::DebugPrintf(TEXT("%s"), szErrMsg);
            LocalFree(szErrMsg);
        }
        else {
            logging::DebugPrintf(TEXT("[Could not find a description for error # %#x.]\n"), hr);
        }
    }
}